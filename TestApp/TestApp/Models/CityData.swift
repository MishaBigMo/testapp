//
//  CityData.swift
//  TestApp
//
//  Created by Михаил on 16/08/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//

import Foundation

class CityData {
    
    var name: String?
    var lat: String?
    var lng: String?
    var dateAndTime: String?
    
}
