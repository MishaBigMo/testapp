//
//  Apimanager.swift
//  TestApp
//
//  Created by Михаил on 15/08/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//
import Alamofire
import Foundation



class ApiManager {
    
    
    // pattern - singleTorn
    static var instance = ApiManager()
    
    func getData(lat: String, lng: String, onComlete: @escaping (Double) -> Void) {
        
        let urlString = "https://api.openweathermap.org/data/2.5/weather?lat=\(lat)&lon=\(lng)&units=metric&APPID=cb0f93438458b0ef8ff48d193b2f6948"

        
        AF.request(urlString, method: .get, parameters: [:]).responseJSON { (respons) in
            switch respons.result {
            case .success(let data):
                print(data)
                var temp = 0.0
                if let arrayWeather = data as? Dictionary<String, Any> {
                    if var main = arrayWeather["main"] as? Dictionary<String, Any> {
                        temp = main["temp"] as? Double ?? 0
                    }
                    onComlete(temp)
                    

                    
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
}

