//
//  MapViewController.swift
//  TestApp
//
//  Created by Михаил on 14/08/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//
import CoreData
import CoreLocation
import MapKit
import UIKit
import Alamofire

class MapViewController: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate {
    
    // MARK: - Properties
    var weather: Weather?
    var openWeatherMapBaseURL = " "
    var lat = ""
    var lng = ""
    var openWeatherMapAPIKey = "cb0f93438458b0ef8ff48d193b2f6948"
    var locationManager = CLLocationManager()
    let regionInMeters: Double = 10000
    var previousLocation: CLLocation?
    let date = Date()
    let dateFormatter = DateFormatter()
    let calendar = Calendar.current
    let formatter = DateFormatter()

    
    // MARK: - Outlets
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var temperature: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    
    
    // MARK: - Actions
    
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        mapView.userTrackingMode = .none
        checkLocationServices()
        
        // Ask for Authorisation from the User.
        self.locationManager.requestAlwaysAuthorization()

        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()

        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }

        if let coor = mapView.userLocation.location?.coordinate{
            mapView.setCenter(coor, animated: true)
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func touchesBegan(_ touches: Set<UITouch>,
                               with event: UIEvent?) {
        self.view.endEditing(true)
    }
    //ok

    func setupLocationManager() {
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
    }
    //ok

    func centerViewOnUserLocation() {
        if let location = locationManager.location?.coordinate {
            let region = MKCoordinateRegion.init(center: location, latitudinalMeters: regionInMeters, longitudinalMeters: regionInMeters)
            //mapView.setRegion(region, animated: true)
        }
    }
    //ok

    func checkLocationServices() {
        if CLLocationManager.locationServicesEnabled() {
            setupLocationManager()
            checkLocationAuthorization()
        } else {
            // Show alert letting the user know they have to turn this on.
        }
    }
    //ok

    func checkLocationAuthorization() {
        switch CLLocationManager.authorizationStatus() {
        case .authorizedWhenInUse:
            startTackingUserLocation()
        case .denied:
            // Show alert instructing them how to turn on permissions
            break
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
        case .restricted:
            // Show an alert letting them know what's up
            break
        case .authorizedAlways:
            break
        }
    }
    //ok

    func startTackingUserLocation() {
        mapView.showsUserLocation = true
        centerViewOnUserLocation()
        locationManager.startUpdatingLocation()
        previousLocation = getCenterLocation(for: mapView)
    }
    
    //ok

    func getCenterLocation(for mapView: MKMapView) -> CLLocation {
        
        let latitude = mapView.centerCoordinate.latitude
        let longitude = mapView.centerCoordinate.longitude
        
        return CLLocation(latitude: latitude, longitude: longitude)
        
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        //ok
        checkLocationAuthorization()

        let locValue:CLLocationCoordinate2D = manager.location!.coordinate

        mapView.mapType = MKMapType.standard

        let span = MKCoordinateSpan(latitudeDelta: 0.05, longitudeDelta: 0.05)
        let region = MKCoordinateRegion(center: locValue, span: span)
        mapView.setRegion(region, animated: true)
        
        let annotation = MKPointAnnotation()
        annotation.coordinate = locValue
        annotation.title = "You"
        annotation.subtitle = "Your location"
        mapView.addAnnotation(annotation)

        let location = locations.first
        let center = CLLocationCoordinate2D(latitude: location!.coordinate.latitude, longitude: location!.coordinate.longitude)
        lat = "\(center.latitude)"
        lng = "\(center.longitude)"

        ApiManager.instance.getData(lat: lat, lng: lng) { data in
            self.temperature.text = "\(data)℃"
            self.locationManager.stopUpdatingLocation()
        }
        
        
        
    }
    

    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        let center = getCenterLocation(for: mapView)
        let geoCoder = CLGeocoder()

       // guard let previousLocation = self.previousLocation else { return }

        //guard center.distance(from: previousLocation) > 50 else { return }
        //self.previousLocation = center

        geoCoder.reverseGeocodeLocation(center) { [weak self] (placemarks, error) in
            guard let self = self else { return }

            if let _ = error {
                //TODO: Show alert informing the user
                return
            }

            guard let placemark = placemarks?.first else {
                //TODO: Show alert informing the user
                return
            }

            let streetNumber = placemark.subThoroughfare ?? ""
            let streetName = placemark.thoroughfare ?? ""

            DispatchQueue.main.async {
                self.locationLabel.text = "\(streetNumber) \(streetName)"
            }
        }
    }
    
    
}




extension MapViewController: UISearchBarDelegate {
    public func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {

        var cities: [CityData] = []
        dateFormatter.dateStyle = .long
        let dateString = "\(dateFormatter.string(from: Date() as Date))"
        let hour = calendar.component(.hour, from: date)
        let minutes = calendar.component(.minute, from: date)

        let searchRequest = MKLocalSearch.Request()
        searchRequest.naturalLanguageQuery = searchBar.text
        let activeSearch = MKLocalSearch(request: searchRequest)
        activeSearch.start {(responce, error) in
            if responce == nil {
                print("ERROR!")
            }
            else
            {
                let annotations = self.mapView.annotations
                self.mapView.removeAnnotations(annotations)

                let latitude = responce?.boundingRegion.center.latitude
                let longitude = responce?.boundingRegion.center.longitude

                let annotation = MKPointAnnotation()
                annotation.title = searchBar.text
                print("\(searchBar.text), \(latitude), \(longitude)")

                let city = CityData()
                city.dateAndTime = "\(dateString), \(hour):\(minutes)"
                city.name = "\(searchBar.text)"
                city.lat = "\(latitude)"
                city.lng = "\(longitude)"
                cities.append(city)


                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                let context = appDelegate.persistentContainer.viewContext
                let entity = NSEntityDescription.entity(forEntityName: "City", in: context)
                let cityObject = NSManagedObject(entity: entity!, insertInto: context) as! City
                if let name = searchBar.text {
                    cityObject.name = name
                }
                if let lat = latitude {
                    cityObject.lat = "\(lat)"
                }
                if let lng = longitude {
                    cityObject.lng = "\(lng)"
                }

                cityObject.dateAndTime = "\(dateString), \(hour):\(minutes)"

                do {
                    try context.save()
                   // cities.append(cityObject)
                    print("Saved! Good Job!")
                } catch {
                    print(error.localizedDescription)
                }

                annotation.coordinate = CLLocationCoordinate2D(latitude: latitude!, longitude: longitude!)
                self.mapView.addAnnotation(annotation)

                let coordinate:CLLocationCoordinate2D = CLLocationCoordinate2DMake(latitude!, longitude!)
                let span = MKCoordinateSpan(latitudeDelta: 0.1, longitudeDelta: 0.1)
                let region = MKCoordinateRegion(center: coordinate, span: span)
                self.mapView.setRegion(region, animated: true)

            }
        }
    }
}
