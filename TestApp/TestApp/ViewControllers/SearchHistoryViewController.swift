//
//  SearchHistoryViewController.swift
//  TestApp
//
//  Created by Михаил on 16/08/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//

import UIKit

class SearchHistoryViewController: UIViewController {
    // MARK: - Properties
    var cityData = City()
    var delegate: ViewController?
    // MARK: - Outlets
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var location: UILabel!
    @IBOutlet weak var time: UILabel!
    
    
    // MARK: - Actions
    
    @IBAction func backTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        name.text = cityData.name
        if let lat = cityData.lat, let lng = cityData.lng {
        location.text = "\(lat) \n \(lng)"
        }
        if let cityTime = cityData.dateAndTime {
        time.text = "\(cityTime)"
        }
        
    }
    
   
    



}
