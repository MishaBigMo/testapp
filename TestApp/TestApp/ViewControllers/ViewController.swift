//
//  ViewController.swift
//  TestApp
//
//  Created by Михаил on 14/08/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//
import CoreData
import UIKit

class ViewController: UIViewController {
    // MARK: - Properties
    var cities: [City] = []
    
    // MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - Actions
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let fetchRequest: NSFetchRequest<City> = City.fetchRequest()
        do {
            cities = try context.fetch(fetchRequest)
            tableView.reloadData()
        } catch {
            print(error.localizedDescription)
        }
    }
}


extension ViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cities.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as! TableViewCell
        let city = cities[indexPath.row]
        
        if let date = city.dateAndTime {
        cell.label1.text = "\(date)"
        }
        if let name = city.name {
        cell.label2.text = "\(name)"
        }
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let searchVC = UIViewController.getFromStoryboard(withId: "SearchHistoryViewController") as! SearchHistoryViewController
        searchVC.cityData = cities[indexPath.row]
        searchVC.delegate = self
        navigationController?.pushViewController(searchVC, animated: true)
    }
    
}
