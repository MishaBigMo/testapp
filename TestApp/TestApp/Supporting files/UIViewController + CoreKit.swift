//
//  UIViewController + CoreKit.swift
//  TestApp
//
//  Created by Михаил on 16/08/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//

import UIKit

extension UIViewController {
    static func getFromStoryboard(withId id: String) -> UIViewController? {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: id)
        return controller
    }
}
