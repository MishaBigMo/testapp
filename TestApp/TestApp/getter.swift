//
//  getter.swift
//  TestApp
//
//  Created by Михаил on 15/08/2019.
//  Copyright © 2019 Михаил. All rights reserved.
//

import Foundation

class WeatherGetter {
    
    private let openWeatherMapBaseURL = "api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}"
    private let openWeatherMapAPIKey = "cb0f93438458b0ef8ff48d193b2f6948"
    
    func getWeather(city: String) {
        
        // This is a pretty simple networking task, so the shared session will do.
        let session = URLSession.shared
        
        let weatherRequestURL = NSURL(string: "api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}")
        
        // The data task retrieves the data.

        
        let dataTask = session.dataTask(with: weatherRequestURL! as URL) {
            (data, response, error) in
            if let error = error {
                // Case 1: Error
                // We got some kind of error while trying to get data from the server.
                print("Error:\n\(error)")
            }
            else {
                // Case 2: Success
                // We got a response from the server!
                print("Raw data:\n\(data!)\n")
                let dataString = String(data: data!, encoding: String.Encoding.utf8)
                print("Human-readable data:\n\(dataString!)")
            }
        }
        
        // The data task is set up...launch it!
        dataTask.resume()
    }
    
}
